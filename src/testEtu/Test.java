package testEtu;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import activeRecord.DBConnection;

public class Test {

	private static DBConnection dbc; 
	
	@Before
	public void init() {
		dbc = DBConnection.getInstance();
	}
	
	@org.junit.Test
	public void test() {
		dbc.setNomDB("test");
		
		DBConnection dbc2 = DBConnection.getInstance();
		DBConnection dbc3 = DBConnection.getInstance();
		
		assertTrue("Les deux objets devraient �tre �gaux", dbc.getConnection().equals(dbc2.getConnection()));
		assertTrue("Les deux objets devraient �tre �gaux", dbc2.equals(dbc3));
	}

}
