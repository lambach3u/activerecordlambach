package activeRecord;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException;

public class Film {

	private int id, id_rea;
	private String titre;

	public Film(String title, Personne p) {
		id = -1;
		id_rea = p.getId();
		titre = title;
	}

	private Film(int ids, String title, int real) {
		id = ids;
		titre = title;
		id_rea = real;
	}

	public static Film findById(int idSearched) {
		try {
			DBConnection dbc = DBConnection.getInstance();

			PreparedStatement ps = dbc.getConnection().prepareStatement("SELECT * FROM Film WHERE id=?;");

			ps.setInt(1, idSearched);

			ResultSet rs = ps.executeQuery();

			String titre = "";
			int id_rea = 0;

			while (rs.next()) {
				titre = rs.getString("titre");
				id_rea = rs.getInt("id_rea");
			}

			Personne p = Personne.findById(id_rea);

			return new Film(titre, p);

		} catch (MySQLSyntaxErrorException e) {
			System.err.println("Probl�me de syntaxe");
			return null;
		} catch (SQLException e1) {
			System.err.println("Impossible de se connecter � la base de donn�es");
			return null;
		}
	}

	public Personne getRealisateur() {
		return Personne.findById(id_rea);
	}

	public static void createTable() {
		try {
			DBConnection dbc = DBConnection.getInstance();

			dbc.getConnection()
					.prepareCall("CREATE TABLE IF NOT EXISTS `film` (" + "`ID` int(11) NOT NULL AUTO_INCREMENT,"
							+ "`TITRE` varchar(40) NOT NULL," + "`ID_REA` int(11) DEFAULT NULL,"
							+ "  PRIMARY KEY (`ID`)," + "  KEY `ID_REA` (`ID_REA`)"
							+ ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;")
					.execute();
		} catch (MySQLSyntaxErrorException e) {
			System.err.println("Probl�me de syntaxe");
		} catch (SQLException e1) {
			System.err.println("Impossible de se connecter � la base de donn�es");
		}
	}
	
	public static void deleteTable() {
		try {
			DBConnection dbc = DBConnection.getInstance();

			Statement stmt = dbc.getConnection().createStatement();

			stmt.executeUpdate("SET foreign_key_checks = 0;");
			stmt.executeUpdate("DROP TABLE FILM;");
			stmt.executeUpdate("SET foreign_key_checks = 0;");

			stmt.close();
		} catch (MySQLSyntaxErrorException e) {
			System.err.println("Probl�me de syntaxe : \n===> " + e.getMessage());
		} catch (SQLException e1) {
			e1.printStackTrace();
			System.err.println("Impossible de se connecter � la base de donn�es : \n===> " + e1.getMessage());
		}
	}
	
	public void save() {
		try {
			if (this.id == -1) {
				this.saveNew();
			} else {
				this.update();
			}

		} catch (MySQLSyntaxErrorException e) {
			System.err.println("Probl�me de syntaxe");
		} catch (SQLException e1) {
			System.err.println("Impossible de se connecter � la base de donn�es");
		} catch (RealisateurAbsentException e) {
			System.err.println("R�alisateur Absent");
		}
	}

	private void update() throws MySQLSyntaxErrorException, SQLException, RealisateurAbsentException {
		if (id != -1) {
			DBConnection dbc = DBConnection.getInstance();

			PreparedStatement ps = dbc.getConnection()
					.prepareStatement("UPDATE FILM SET TITRE=?, ID_REA=? WHERE ID=?;");

			ps.setString(1, titre);
			ps.setInt(2, id_rea);
			ps.setInt(3, id);

			if(id_rea != -1) {
				ps.execute();	
			} else {
				throw new RealisateurAbsentException();
			}
			
			
			ps.close();
		}
	}

	private void saveNew() throws MySQLSyntaxErrorException, SQLException, RealisateurAbsentException {
		if (id == -1) {
			DBConnection dbc = DBConnection.getInstance();

			PreparedStatement ps = dbc.getConnection().prepareStatement(
					"INSERT INTO FILM (TITRE, ID_REA) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);

			ps.setString(1, titre);
			ps.setInt(2, id_rea);

			ps.executeUpdate();

			// recuperation de la derniere ligne ajoutee (auto increment)
			// recupere le nouvel id
			int autoInc = -1;
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				autoInc = rs.getInt(1);
			}

			this.id = autoInc;
			
			if (id == -1) throw new RealisateurAbsentException();

			rs.close();
			ps.close();
		}
	}

	public int getId() {
		return id;
	}

	public int getId_rea() {
		return id_rea;
	}

	public String getTitre() {
		return titre;
	}

	public void setId_rea(int id_rea) {
		this.id_rea = id_rea;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	
	

}
