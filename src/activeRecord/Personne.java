package activeRecord;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException;

public class Personne {

	private int id;
	private String nom, prenom;

	public Personne(String name, String pn) {
		id = -1;
		nom = name;
		prenom = pn;
	}

	public static ArrayList<Personne> findAll() {
		ArrayList<Personne> list = new ArrayList<Personne>();

		try {
			ResultSet rs = getResultSet();
			// s'il y a un resultat
			while (rs.next()) {
				String nom = rs.getString("nom");
				String prenom = rs.getString("prenom");
				int id = rs.getInt("id");
				Personne p = new Personne(nom, prenom);
				p.id = id;
				list.add(p);
			}
			rs.close();
			
			return list;
		} catch (SQLException e) {
			System.err.println("Impossible de se connecter � la base de donn�es");
		}
		return null;
	}

	public static Personne findById(int idSearched) {
		try {
			DBConnection dbc = DBConnection.getInstance();
			
			PreparedStatement ps = dbc.getConnection().prepareStatement("SELECT * FROM Personne WHERE id=?");
			
			ps.setInt(1, idSearched);
			
			ResultSet rs = ps.executeQuery();
				
				if (rs.next()) {
					Personne p = new Personne(rs.getString("nom"), rs.getString("prenom"));
					p.id = idSearched;
					return p;
				}

				ps.close();
				rs.close();
				
		} catch (MySQLSyntaxErrorException e) {
			System.err.println("Probl�me de syntaxe");
		} catch (SQLException e1) {
			System.err.println("Impossible de se connecter � la base de donn�es");
		}
		return null;
	}

	public static ArrayList<Personne> findByName(String nameSearched) {
		try {
			DBConnection dbc = DBConnection.getInstance();
			
			PreparedStatement ps = dbc.getConnection().prepareStatement("SELECT * FROM Personne WHERE nom=?;");

			ps.setString(1, nameSearched);
			
			ResultSet rs = ps.executeQuery();
			
			ArrayList<Personne> list = new ArrayList<Personne>();

			while (rs.next()) {
				list.add(new Personne(rs.getString("nom"), rs.getString("prenom")));
			}

			ps.close();
			rs.close();
			
			return list;
		} catch (MySQLSyntaxErrorException e) {
			System.err.println("Probl�me de syntaxe");
			return null;
		} catch (SQLException e1) {
			System.err.println("Impossible de se connecter � la base de donn�es");
			return null;
		}
	}

	public void save() {
		try {
			if (this.id == -1) {
				this.saveNew();
			} else {
				this.update();
			}

		} catch (MySQLSyntaxErrorException e) {
			System.err.println("Probl�me de syntaxe");
		} catch (SQLException e1) {
			System.err.println("Impossible de se connecter � la base de donn�es");
		}
	}

	private void update() throws MySQLSyntaxErrorException, SQLException {
		if (id != -1) {
			DBConnection dbc = DBConnection.getInstance();

			PreparedStatement ps = dbc.getConnection()
					.prepareStatement("UPDATE PERSONNE SET NOM=?, PRENOM=? WHERE ID=?;");

			ps.setString(1, nom);
			ps.setString(2, prenom);
			ps.setInt(3, id);

			ps.execute();

			ps.close();
		}
	}

	private void saveNew() throws MySQLSyntaxErrorException, SQLException {
		if (id == -1) {
			DBConnection dbc = DBConnection.getInstance();

			PreparedStatement ps = dbc.getConnection().prepareStatement(
					"INSERT INTO PERSONNE (NOM, PRENOM) " + "VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);

			ps.setString(1, nom);
			ps.setString(2, prenom);

			ps.executeUpdate();

			// recuperation de la derniere ligne ajoutee (auto increment)
			// recupere le nouvel id
			int autoInc = -1;
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				autoInc = rs.getInt(1);
			}

			this.id = autoInc;

			rs.close();
			ps.close();
		}
	}

	public void delete() {
		try {
			DBConnection dbc = DBConnection.getInstance();

			PreparedStatement ps = dbc.getConnection().prepareStatement("DELETE FROM PERSONNE WHERE ID=?");
			ps.setInt(1, id);
			
			ps.execute();
			
			id = -1;

			ps.close();
			
		} catch (MySQLSyntaxErrorException e) {
			System.err.println("Probl�me de syntaxe");
		} catch (SQLException e1) {
			e1.printStackTrace();
			System.err.println("Impossible de se connecter � la base de donn�es");
		}
	}

	public static void createTable() {
		try {
			DBConnection dbc = DBConnection.getInstance();

			dbc.getConnection()
					.prepareCall("CREATE TABLE IF NOT EXISTS `personne` (\r\n"
							+ "  `ID` int(11) NOT NULL AUTO_INCREMENT,\r\n" + "  `NOM` varchar(40) NOT NULL,\r\n"
							+ "  `PRENOM` varchar(40) NOT NULL,\r\n" + "  PRIMARY KEY (`ID`)\r\n"
							+ ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;")
					.execute();
		} catch (MySQLSyntaxErrorException e) {
			System.err.println("Probl�me de syntaxe");
		} catch (SQLException e1) {
			System.err.println("Impossible de se connecter � la base de donn�es");
		}
	}

	public static void deleteTable() {
		try {
			DBConnection dbc = DBConnection.getInstance();

			Statement stmt = dbc.getConnection().createStatement();

			stmt.executeUpdate("SET foreign_key_checks = 0;");
			stmt.executeUpdate("DROP TABLE PERSONNE;");
			stmt.executeUpdate("SET foreign_key_checks = 0;");

			stmt.close();
		} catch (MySQLSyntaxErrorException e) {
			System.err.println("Probl�me de syntaxe : \n===> " + e.getMessage());
		} catch (SQLException e1) {
			e1.printStackTrace();
			System.err.println("Impossible de se connecter � la base de donn�es : \n===> " + e1.getMessage());
		}
	}

	public static ResultSet getResultSet() throws SQLException {
		DBConnection dbc = DBConnection.getInstance();

		String SQLPrep = "SELECT * FROM Personne";

		PreparedStatement prep1 = dbc.getConnection().prepareStatement(SQLPrep);
		prep1.execute();
//
//		prep1.close();
		
		return prep1.getResultSet();
	}

	public String toString() {
		return " -> (" + id + ") " + prenom + ", " + nom;
	}

	public int getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

}
