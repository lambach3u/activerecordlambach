package activeRecord;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection {

	private static DBConnection instance;

	private Connection connection;

	private String userName, password, serverName, portNumber, tableName, dbName;
	
	private DBConnection() {
		// variables a modifier en fonction de la base
		userName = "root";
		password = "";
		serverName = "localhost";
		// Attention, sous MAMP, le port est 8889
		portNumber = "3306";
		tableName = "personne";

		// iL faut une base nommee testPersonne !
		dbName = "testPersonne";
		
		// creation de la connection
		Properties connectionProps = new Properties();
		connectionProps.put("user", userName);
		connectionProps.put("password", password);
		String urlDB = "jdbc:mysql://" + serverName + ":";
		urlDB += portNumber + "/" + dbName;
		try {
			this.connection = DriverManager.getConnection(urlDB, connectionProps);
		} catch (SQLException e) {
			this.connection = null;
		}
		
	
	}

	public static DBConnection getInstance() {
		return (instance == null) ? instance = new DBConnection() : instance;
	}

	public Connection getConnection() {
		return connection;
	}

	public void setNomDB(String nomDB) {
		try {
			Properties connectionProps = new Properties();
			connectionProps.put("user", userName);
			connectionProps.put("password", password);
			String urlDB = "jdbc:mysql://" + serverName + ":";
			urlDB += portNumber + "/" + nomDB;
			this.connection = DriverManager.getConnection(urlDB, connectionProps);
			this.dbName = nomDB;
			System.out.println("Connect� � la nouvelle base de donn�e "+dbName);
		} catch (SQLException e) {
			System.err.println("Echec du changement de la base de donn�e "+nomDB+". Retour sur "+dbName);
		}
	}

}
